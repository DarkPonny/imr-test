class StocksController < ApplicationController
  before_action :find_stock, except: :index
  before_action :find_stock_product, only: [:move_product_to, :sell_product]

  def index
    render json: Stock.all
  end

  def show
    render json: @stock.to_json(include: { stock_products: { include: :product } })
  end

  def move_product_to
    target_stock = Stock.find_by_id(move_params[:target_stock_id])

    return render json: { error: 'Product not found on stock', code: 1 } if @stock_product.blank?

    if move_params[:quantity].to_i > @stock_product.quantity
      return render json: { error: "Not enough product on stock. Available - #{@stock_product.quantity}", code: 2 }
    end

    return render json: { error: "Target stock with id=#{move_params[:target_stock_id]} not found", code: 3 } if target_stock.blank?

    @stock.move_products!(move_params[:product_id], target_stock, move_params[:quantity])

    render head: :ok
  end

  def sell_product
    return render json: { error: 'Product not found on stock', code: 1 } if @stock_product.blank?

    if move_params[:quantity].to_i > @stock_product.quantity
      return render json: { error: "Not enough product on stock. Available - #{@stock_product.quantity}", code: 2 }
    end

    @stock.sell_product!(@stock_product.product_id, sell_params[:quantity])

    render head: :ok
  end

private

  def find_stock_product
    @stock_product ||= @stock.stock_products.find_by(product_id: move_params[:product_id])
  end

  def find_stock
    @stock ||= Stock.find(params[:id])
  end

  def sell_params
    params.permit(:product_id, :quantity)
  end

  def move_params
    params.permit(:product_id, :quantity, :target_stock_id)
  end
end
