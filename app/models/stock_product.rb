class StockProduct < ApplicationRecord
  belongs_to :stock
  belongs_to :product

  validates_uniqueness_of :product_id, scope: :stock_id
end
