class Stock < ApplicationRecord
  has_many :stock_products, dependent: :destroy
  has_many :products, through: :stock_products, dependent: :destroy

  def move_products!(product_id, target_stock, quantity)
    ActiveRecord::Base.transaction do
      moved_product = target_stock.stock_products.find_or_create_by(product_id: product_id)
      moved_product.update(quantity: moved_product.quantity + quantity)

      stock_product = stock_products.find_by!(product_id: product_id)
      stock_product.update(quantity: stock_product.quantity - quantity)
    end
  end

  def sell_product!(product_id, quantity)
    ActiveRecord::Base.transaction do
      stock_product = stock_products.find_by!(product_id: product_id)
      stock_product.update(quantity: stock_product.quantity - quantity)
      update_attribute(:balance, balance + (quantity * stock_product.product.price))
    end
  end
end
