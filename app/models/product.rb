class Product < ApplicationRecord
  has_many :stock_products
  has_many :stocks, through: :stock_products
end
