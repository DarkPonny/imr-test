# README

# test application

# To start
  - rails db:create
  - rails db:migrate
  - rails db:seed
  - rails s

# Test product moving through stocks
Get stock list
```bash
curl --location --request GET 'localhost:3000/stocks/'
```
Then select id one of stock in response and send request to get all of his products
```bash
curl --location --request GET 'localhost:3000/stocks/:stock_id/'
```
Now select id one of stock product and move it to another stock
```bash
curl --location --request POST 'localhost:3000/stocks/:stock_id/move_product_to' \
--header 'Content-Type: application/json' \
--data-raw '{
    "product_id": 3,
    "quantity": 10,
    "target_stock_id": 10 # where to move product
}'
```
And now u can check changes using this query
```bash
curl --location --request GET 'localhost:3000/stocks/:stock_id/'
```

# Test product selling
Get stock list
```bash
curl --location --request GET 'localhost:3000/stocks/'
```
Then select id one of stock in response and send request to get all of his products
```bash
curl --location --request GET 'localhost:3000/stocks/:stock_id/'
```
Now select id one of stock product and sell it
```bash
curl --location --request POST 'localhost:3000/stocks/:stock_id/sell_product' \
--header 'Content-Type: application/json' \
--data-raw '{
    "product_id": 3,
    "quantity": 10,
    "target_stock_id": 7
}'
```
And now u can check changes (stock balance and product quantity) using this query
```bash
curl --location --request GET 'localhost:3000/stocks/:stock_id/'
```
