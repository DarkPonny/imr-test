# Create stocks
5.times do |i|
  Stock.create(name: Faker::FunnyName.four_word_name, address: Faker::Address.full_address)
end

# Create products
5.times do |i|
  Product.create(name: Faker::Commerce.product_name, price: Faker::Commerce.price)
end

# Create stock products
Stock.find_each do |stock|
  3.times do
    StockProduct.create(stock_id: stock.id, product_id: Product.pluck(:id).sample, quantity: rand(1..1000))
  end
end
