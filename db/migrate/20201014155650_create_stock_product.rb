class CreateStockProduct < ActiveRecord::Migration[6.0]
  def change
    create_table :stock_products do |t|
      t.references :stock, foreign_key: true, dependent: :destroy
      t.references :product, foreign_key: true, dependent: :destroy
      t.integer :quantity, null: false, default: 0
    end
  end
end
