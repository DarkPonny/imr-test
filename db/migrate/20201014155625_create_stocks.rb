class CreateStocks < ActiveRecord::Migration[6.0]
  def change
    create_table :stocks do |t|
      t.string :name, null: false
      t.string :address, null: false
      t.decimal :balance, precision: 9, scale: 2, null: false, default: 0
      t.timestamps
    end
  end
end
