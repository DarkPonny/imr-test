class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.decimal :price, precision: 9, scale: 2, null: false, default: 0
      t.timestamps
    end
  end
end
