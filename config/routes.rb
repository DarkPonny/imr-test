Rails.application.routes.draw do
  resources :stocks, only: [:show, :index] do
    post :sell_product, on: :member
    post :move_product_to, on: :member
  end
end
